import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicFormComponent } from './Forms/dynamic-form/dynamic-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormTextComponent } from './Components/Questiontypes/form-text/form-text.component';
import { FormSelectComponent } from './Components/Questiontypes/form-select/form-select.component';
import { RxReactiveDynamicFormsModule } from '@rxweb/reactive-dynamic-forms';

@NgModule({
  declarations: [
    AppComponent,
    DynamicFormComponent,
    FormTextComponent,
    FormSelectComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveDynamicFormsModule,
    RxReactiveFormsModule
  ],
  exports: [
    FormTextComponent,
    FormSelectComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor() {
  }
}
// platformBrowserDynamic().bootstrapModule(AppModule);
