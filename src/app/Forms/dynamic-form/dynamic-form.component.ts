import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Optional } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { element } from 'protractor';
import { Observable, of } from 'rxjs';
import { QuestionControlService } from 'src/app/Components/Formbuilder/question-control.service';
import { QuestionService } from 'src/app/Components/Formbuilder/question.service';
import { QuestionBase } from 'src/app/Components/Formbuilder/QuestionBase';
import questionsData from '../../../assets/questionsFile.json';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css'],
  providers: [QuestionControlService],
})
export class DynamicFormComponent implements OnInit {
  // @Input() selectedLanguage!: string;
  @Input() openForm!: boolean;
  @Input() languageSelectedObj: any[] = [];
  @Input() LanguageLabelSelected: any[];
  payLoad = '';
  languageSelectVar: string;
  questions: any[] = questionsData;
  displayButtonGenerate: boolean = true;
  showChangeLanguageButton: boolean = false;
  languageLabelVar: string;
  languageArray2: any[] = [];
  form: FormGroup = this.fb.group({});
  questionsForm: FormGroup = this.fb.group({});
  questionsVar: any[];
  forParentHtml:any;


  constructor(@Optional() private fb: FormBuilder, @Optional() private httpClient: HttpClient) {
    // this.questions = [{
    //   label
    // }]
    this.form = new FormGroup({
      languageSelectName: new FormControl(),
    });

  }

  ngOnInit() {
    this.forParentHtml = document.getElementById('questionForm');
    // this.getFields(this.questionsForm, this.questionsVar, '../../../assets/questionsFile.json')
    this.questions.forEach((item) => {
      item.newLabel.forEach((element) => {
        const key = Object.keys(element);
        for (let index = 0; index < key.length; index++) {
          const element = key[index];
          this.languageArray2.push(element);
        }
      });
    });
    this.languageArray2 = this.removeDuplicates(this.languageArray2);
  }

  removeDuplicates(array) {
    return array.filter((value, index) => array.indexOf(value) === index);
  }

  onButtonClick() {
    this.payLoad = JSON.stringify(this.questionsForm.getRawValue());

  }

  onLanguageChangeClick() {
    this.showChangeLanguageButton = false;
    this.openForm = false;
    this.displayButtonGenerate = true;
    this.questionsForm.reset();
    this.questionsForm = this.fb.group({});
    this.questionsVar = [];
    this.payLoad = null;
    console.log(this.questionsForm);
  }



  createForm(questions: any[], formParam) {

    for (const question of questions) {

      // formParam = this.fb.group(question.key question.required === true ?
      // [
      //   Validators.required,
      // ]
      // :
      // question.type === 'email' && question.required === true
      //   ? [
      //     Validators.required,
      //     Validators.email,
      //     Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      //   ]
      //   : question.type === 'password' && question.required === true
      //     ? [
      //       Validators.required,
      //       Validators.minLength(8),
      //     ]
      //     : question.type === 'email'
      //       ? [
      //         Validators.email,
      //         Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      //       ]
      //       : question.type === 'password'
      //         ? [Validators.minLength(8)]
      //         : []})
      formParam.addControl(
        question.key,

        this.fb.control(question.value || '', question.required === true ?
          [
            Validators.required,
          ]
          :
          question.type === 'email' && question.required === true
            ? [
              Validators.required,
              Validators.email,
              Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
            ]
            : question.type === 'password' && question.required === true
              ? [
                Validators.required,
                Validators.minLength(8),
              ]
              : question.type === 'email'
                ? [
                  Validators.email,
                  Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
                ]
                : question.type === 'password'
                  ? [Validators.minLength(8)]
                  : [])
      );
    }
  }

  getFields(language, formParam, jsonParam) {
    this.httpClient.get(jsonParam)
      .pipe(map((fields: QuestionBase<any>[]) => {
        return fields.map(field => {
          return new QuestionBase<any>(field)
        })
      }))
      .subscribe((fields: any) => {
        this.questionsVar = fields;
        this.createForm(this.questionsVar, formParam)
        this.questionsVar.forEach((item) => {
          for (let index = 0; index < item.newLabel.length; index++) {
            const element = item.newLabel[index];
            const key = Object.keys(element);
            this.LanguageLabelSelected = Object.keys(element);
            if (language == key) {
              this.languageLabelVar = element[language];
              if (
                element.LanguageLabelSelected == undefined ||
                element.LanguageLabelSelected == null
              ) {
                item.newLabel.push({
                  LanguageLabelSelected: this.languageLabelVar,
                });
              }
            }
          }
        });
      });
  }

  public generateForm(language: any, jsonRead: any) {
    // this.generateQuestions()
    this.onLanguageChangeClick()
    this.getFields(language, this.questionsForm, jsonRead)
    this.showChangeLanguageButton = true;
    this.displayButtonGenerate = false;
    this.openForm = true;

    return this.questionsForm
  }
}
