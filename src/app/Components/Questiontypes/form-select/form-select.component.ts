import {
  Component,
  Inject,
  Input,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';
import {
  NgModel,
  NG_VALIDATORS,
  NG_ASYNC_VALIDATORS,
  NG_VALUE_ACCESSOR,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { animations, ElementBase } from '../../Formbuilder';

@Component({
  selector: 'form-select',
  styleUrls: ['./form-select.component.css'],
  templateUrl: './form-select.component.html',
  animations,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormSelectComponent,
      multi: true,
    },
  ],
})
export class FormSelectComponent {
  @Input() public placeholder: string;
  @Input() form: FormGroup;
  @Input() questionComp: any;

  @ViewChild(NgModel) model: NgModel;

  public identifier = `form-select-${identifier++}`;

  constructor(@Optional() @Inject(NG_VALIDATORS) validators: Array<any>,@Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,) {
  }
}
let identifier = 0;
