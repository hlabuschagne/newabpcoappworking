import { Component, Optional, Inject, Input, ViewChild } from '@angular/core';

import {
  NgModel,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  NG_ASYNC_VALIDATORS,
  FormControl,
  FormGroup,
} from '@angular/forms';

import {
  ElementBase,
  animations,
  ValueAccessorBase,
} from '../../Formbuilder';

@Component({
  selector: 'form-text',
  styleUrls: ['./form-text.component.css'],
  templateUrl: './form-text.component.html',
  animations,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormTextComponent,
      multi: true,
    },
  ],
})
export class FormTextComponent extends ElementBase<string> {
  @Input() form: FormGroup;
  @Input() questionComp: any;
  @ViewChild(NgModel) model: NgModel;

  public identifier = `form-text-${identifier++}`;

  get isValid() {
    return  this.form.controls[this.questionComp.key].valid;
  }

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators?: Array<any>
  ) {
    super(validators, asyncValidators);
  }
}

let identifier = 0;
