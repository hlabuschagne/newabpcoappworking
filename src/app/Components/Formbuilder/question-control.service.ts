import { ElementRef, Injectable } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { QuestionBase } from './QuestionBase';

@Injectable()
export class QuestionControlService {
  constructor(private elRef: ElementRef) {}
  toFormGroup(questions) {
    const group: any = {};

    questions.forEach((question) => {
      group[question.key] = question.required
        ? question.type === 'email'
          ? new FormControl(question.value || '', [
              Validators.required,
              Validators.email,
              Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
            ])
          : question.type === 'password'
          ? new FormControl(question.value || '', [
              Validators.required,
              Validators.minLength(8),
            ])
          : new FormControl(question.value || '', Validators.required)
        : question.type === 'email'
        ? new FormControl(question.value || '', [
            Validators.email,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
          ])
        : question.type === 'password'
        ? new FormControl(question.value || '', [Validators.minLength(8)])
        : new FormControl(question.value || '');
    });
    console.log(group);
    // console.log(this.ngAfterContentInit())
    return new FormGroup(group);
  }

  // ngAfterContentInit() {
  //   this.elRef.nativeElement.labels.forEach((l) => (l.textContent += ' *'));
  // }
}
