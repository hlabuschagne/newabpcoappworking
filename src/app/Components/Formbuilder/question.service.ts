import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';


import { Observable, of } from 'rxjs';
import { QuestionBase } from './QuestionBase';

@Injectable()
export class QuestionService {
  constructor(
    private httpClient: HttpClient
  ) {}

  getQuestions(){
    return this.httpClient.get<QuestionBase<any>[]>("../../../assets/questionsFile.json")
  }

}


