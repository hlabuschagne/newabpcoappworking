export class QuestionBase<T> {
  value: T|undefined;
  key: string;
  name: string;
  DBTable: string;
  newLabel: [
    {Afrikaans: string},
    {English: string},
    {LanguageLabelSelected: any}
  ][]
  label: string;
  DBField: string;
  required: boolean;
  order: number;
  controlType: string;
  type: string;
  options: {key: string, value: string}[];
  validator: string;

  constructor(options: {
      value?: T;
      key?: string;
      validator?: string;
      name?: string;
      label?: string;
      newLabel?: [
        {Afrikaans: string},
        {English: string},
        {LanguageLabelSelected: any[]}
      ][];
      DBTable?: string;
      DBField?: string;
      required?: boolean;
      order?: number;
      controlType?: string;
      type?: string;
      options?: {key: string, value: string}[];
    } = {}) {
    this.value = options.value;
    this.validator = options.validator || '';
    this.name = options.name || '';
    this.key = options.key || '';
    this.newLabel = options.newLabel || [];
    this.DBTable = options.DBTable || '';
    this.DBField = options.DBField || '';
    this.required = !!options.required;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.options = options.options || [];
  }
}
