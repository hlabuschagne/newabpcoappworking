import { HttpClient } from '@angular/common/http';
import { DynamicFormComponent } from './Forms/dynamic-form/dynamic-form.component';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { QuestionControlService } from './Components/Formbuilder/question-control.service';
import { QuestionService } from './Components/Formbuilder/question.service';
import { QuestionBase } from './Components/Formbuilder/QuestionBase';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:  [QuestionService]
})
export class AppComponent implements OnInit{
  title = 'apbco-NewDynamicForm';
  formBuild: FormGroup = new FormGroup({});
  formBuildHerman: FormGroup = new FormGroup({});
  formGroupInside: FormGroup;
  formVariable: any;
  languageVar: string = '';
  languageChosen: boolean = false;
  displayButtonGenerate2: boolean = true;

  @ViewChild(DynamicFormComponent) formChildComp;


  constructor(private fb: FormBuilder, private httpClient: HttpClient) {
  }
  ngOnInit(): void {
    // this.formBuild = new FormGroup(this.formChildComp)
  }

  setHerman() {
    this.languageVar = (<HTMLInputElement>document.getElementById('languageSelect')).value
    this.formBuildHerman = this.formChildComp.generateForm(this.languageVar,'../assets/questionsFile.json');
    console.log(this.formBuildHerman )

  }

  setAdolf() {
    this.languageVar = (<HTMLInputElement>document.getElementById('languageSelect')).value
    this.formBuild = this.formChildComp.generateForm(this.languageVar,'../assets/questionsTest.json');
    console.log(this.formBuild )
  }
  // f = create new form
  // findlement by id adolf
}
